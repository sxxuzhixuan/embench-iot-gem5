XLEN ?= 64

default: all

bmarks = \
	aha-mont64 \
	crc32 \
	cubic  \
	edn  \
	huffbench  \
	matmult-int  \
	minver  \
	nbody  \
	nettle-aes  \
	nettle-sha256 \
	nsichneu  \
	picojpeg  \
	qrduino  \
	sglib-combined  \
	slre  \
	st  \
	statemate  \
	ud  \
	wikisort

support = $(wildcard support/*.c)


#--------------------------------------------------------------------
# Build rules
#--------------------------------------------------------------------
RISCV_PREFIX ?= /opt/riscv-toolchain-bin-imac-9.2.0/bin/riscv$(XLEN)-unknown-elf-
RISCV_GCC ?= $(RISCV_PREFIX)gcc
RISCV_GCC_OPTS ?= -DCPU_MHZ=1 -DWARMUP_HEAT=1 -O2

incs  += -I./support


define compile_template
$(1).riscv: $(./src/$(1)/*.c) $(./support/*.c)
	$$(RISCV_GCC) $$(incs) $$(RISCV_GCC_OPTS) -o $$@ \
	./config/riscv64/boards/generic/boardsupport.c \
	$(wildcard ./src/$(1)/*.c) \
	$(patsubst ./support/dummy-libc.c,, $(patsubst ./support/dummy-crt0.c,,$(patsubst ./support/chip.c,,$(patsubst ./support/board.c,,$(wildcard ./support/*.c)))))
endef

$(foreach bmark,$(bmarks),$(eval $(call compile_template,$(bmark))))

bmarks_riscv_bin = $(addsuffix .riscv,  $(bmarks))

all: $(bmarks_riscv_bin)

clean:
	rm *.riscv


